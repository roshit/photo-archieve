$(document).ready(function() {
    var $checkboxes = $('.stock-image_category--filter input');

    $checkboxes.change(function() {
        var filters = [];
        // get checked checkboxes values
        $checkboxes.filter(':checked').each(function() {
            filters.push(this.value);
        });
        // ['.red', '.blue'] -> '.red, .blue'
        filters = filters.join(', ');
        $('.grid').isotope({
            filter: filters
        });
    });

    // var $grid = $('.grid').isotope({
    //     // options
    //     itemSelector: '.grid-item',
    //     // layoutMode: 'fitRows'
    //     stagger: 30
    // });
    var $grid = $('.grid').imagesLoaded(function() {
        $grid.isotope({
            itemSelector: '.grid-item',
            // percentPosition: true,
            stgger: 30
        });
    });

    $('.filter-button-group').on('click', '.btn', function() {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({
            filter: filterValue
        });
    });
    $('.btn-group').each(function(i, btnGroup) {
        var $btnGroup = $(btnGroup);
        $btnGroup.on('click', '.btn', function() {
            $btnGroup.find('.active').removeClass('active');
            $(this).addClass('active');
        });
    });

    $('ul.stock-image_category li').click(function() {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({
            filter: filterValue
        });
    });
    $('.stock-image_category').each(function(i, imgCategory) {
        var $imgCategory = $(imgCategory);
        $imgCategory.on('click', '.category', function() {
            $imgCategory.find('.active').removeClass('active');
            $(this).addClass('active');
        });
    });

    // https://codepen.io/desandro/pen/WwavbO

	$('.sorting-selection').on( 'change', function() {
		// get filter value from option value
		var filterValue = this.value;
		$grid.isotope({ filter: filterValue });
	});
});
